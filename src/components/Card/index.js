import React, { Component } from 'react';
import EasyStyle from 'react-easy-style';
import css from './style.scss';

@EasyStyle(css)
class Card extends Component {

  static propTypes = {
    'data' : React.PropTypes.string.isRequired,
  }

  render () {
    const card = this.props.data
      .replace('s', '♠')
      .replace('c', '♣')
      .replace('h', '♥')
      .replace('d', '♦')
      .toUpperCase();
      
    this.props.color = this.props.data.indexOf('s') !== -1 || this.props.data.indexOf('c') !== -1 ? 'black' : 'red';

    return (
      <div>
        <div is="item">
          <div is="symbol-top">{card}</div>
          <div is="symbol-bottom">{card}</div>
        </div>
      </div>
    );
  }

}

export default Card;
