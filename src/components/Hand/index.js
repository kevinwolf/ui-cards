import React, { Component } from 'react';
import EasyStyle from 'react-easy-style';
import Card from '../Card';
import css from './style.scss';

@EasyStyle(css)
class Hand extends Component {

  static propTypes = {
    'cards' : React.PropTypes.array.isRequired,
  }

  render () {
    const cards = this.props.cards.map(card => <Card data={card} />);

    return (
      <div>{cards}</div>
    );
  }

}

export default Hand;
