#Cards
Add this module on all *DealerInterfaces* and *PlayerInterfaces* which needs to be able to display a Card.

##Usage
1. Run `npm install apogeelive.ui.cards`.
2. Import the module on the application.
4. Use `import { Card } from 'apogeelive.ui.cards'` to render only a Card.
3. Use `import { Hand } from 'apogeelive.ui.cards'` to render a Hand (set of Cards).

###Example
```javascript
import React, { Component } from 'react';
import { Card, Hand } from 'apogeelive.ui.cards';

class App extends Component {

  render () {
    return (
      <div>
        <h1>A Card</h1>
        <Card data="10h" />
        <h1>A Hand</h1>
        <Hand cards={[ 'as', '2c', '3d', '4h' ]} />
      </div>
    );
  }

}

React.render(<App />, document.body);
```
