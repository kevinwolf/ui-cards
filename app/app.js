import React, { Component } from 'react';
import { Card, Hand } from '../src';

class App extends Component {

  render () {
    return (
      <div>
        <h1>A Card</h1>
        <Card data="10h" />
        <h1>A Hand</h1>
        <Hand cards={[ 'as', '2c', '3d', '4h' ]} />
      </div>
    );
  }

}

React.render(<App />, document.body);
